# Isleward Ansible Playbook

## What is Ansible?

[Ansible](https://www.ansible.com/) is software that automates software provisioning, configuration management, and application deployment in a state-aware manner.

## How do I run it?

First, make sure Ansible is installed on your control machine (can be a desktop). You don't need to install anything on the server that'll be provisioned.  
An Installation guide can be found [here](http://docs.ansible.com/ansible/devel/installation_guide/intro_installation.html). While not officially supported, Ansible can run on Windows 10 under WSL.

When Ansible is installed, edit `playbook.yml` and `inventory.ini` to suit your needs and run the following commands:

```bash
# Clone this repository
git clone git@gitlab.com:Isleward/ansible/isleward-playbook.git

# Enter the repository directory
cd isleward-playbook

# Pull required roles
ansible-galaxy install -r requirements.yml

# Run the playbook
ansible-playbook playbook.yml -i inventory.ini
```

After the playbook is finished running, you need to ssh into the server and run the docker-compose file manually.